INTRODUCTION
------------

The Webform Skip module makes a multi-step webform's progress bar "clickable"
so users can skip to various parts of the form quickly.

 * For a full description of the module, visit the project page:
   http://drupal.org/project/webform_skip

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/webform_skip


REQUIREMENTS
------------

This module requires the following modules:

 * Webform (https://www.drupal.org/project/webform)


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/documentation/install/modules-themes/modules-7
for further information.


CONFIGURATION
-------------

 * When you enable this module, the module's progress skip functionality will
   be turned on for all multi-step webforms. You can disable the functionality
   on a per-webform basis with a setting found in the "Progress Bar" section
   of a webform's configuration form.

 * If you have multiple webforms on your site that you want to disable this
   module's functionality on, you can easily do so on this module's config form
   `/admin/config/content/webform/skip`
