<?php

/**
 * @file
 * Webform Skip module admin configuration form.
 */

/**
 * Admin form to easily en/disable this module's functionality on all webforms.
 */
function webform_skip_disable_form($form, &$form_state) {

  // Add some help text to tell the admin what they are doing.
  $help_url = module_exists('help') ? 'admin/help/webform_skip' :
    'https://www.drupal.org/project/webform_skip';
  $t_args = array('!link' => l(t('Webform Skip'), $help_url));
  $form['help'] = array(
    '#markup' => t('<p>Select which webform(s) you would like to enable
      !link\'s functionality on.</p>
      <p>Checking the box next to a webform listed below will make a multi-step
      webform\'s progress bar "clickable" so users can skip to various parts
      of the form quickly..</p>
      <p><strong>Note:</strong> Not all the webforms below may have progress
      bars. Enabling Webform Skip on those webforms will have
      no effect.', $t_args),
  );

  // Get all webforms that have had this module's functionality disabled.
  $disabled_webforms = variable_get('webform_skip_disabled', array());

  // Get list of webform-enabled content types.
  $types = webform_variable_get('webform_node_types', array());

  // Get list of all webform nodes.
  $result = db_select('node', 'n')
    ->fields('n', array('nid', 'title'))
    ->condition('type', $types, 'IN')
    ->condition('status', 1, '=')
    ->orderBy('nid')
    ->execute();

  $options = array();
  $default_value = array();
  foreach ($result as $node) {
    // If this node ID is not disabled, set is as CHECKED
    // checking the box will enable the skipping functionality.
    if (!in_array($node->nid, $disabled_webforms)) {
      $default_value[$node->nid] = $node->nid;
    }

    // Add this webform as an available option to the tableselect element.
    $options[$node->nid] = array('nid' => $node->nid, 'title' => $node->title);
  }

  $header = array(
    'title' => t('Webform Name'),
  );
  $form['nodes'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#default_value' => $default_value,
    '#empty' => t('There are no webforms defined in the system.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Form submission handler for the admin form.
 */
function webform_skip_disable_form_submit($form, &$form_state) {
  $value = $form_state['values']['nodes'];

  // Get which webforms the person selected.
  $selected_webforms = array_filter($value);
  // Now find the difference between what the person selected,
  // and all the possible values
  // this will be the webforms they unchecked i.e. disabled.
  $disabled_webforms = array_diff($value, $selected_webforms);

  // Go through the disabled webforms, and set the value to the {node}.nid
  // since it will be set to 0 since the value is unchecked.
  foreach ($disabled_webforms as $nid => $v) {
    $disabled_webforms[$nid] = $nid;
  }

  // Set the variable to disable the webforms they unchecked.
  variable_set('webform_skip_disabled', $disabled_webforms);
}
